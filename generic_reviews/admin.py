# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Category, Review


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    pass
