# -*- coding: utf-8 -*-
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)

from .models import Review


class ReviewCreateView(CreateView):

    model = Review


class ReviewDeleteView(DeleteView):

    model = Review


class ReviewDetailView(DetailView):

    model = Review


class ReviewUpdateView(UpdateView):

    model = Review


class ReviewListView(ListView):

    model = Review
