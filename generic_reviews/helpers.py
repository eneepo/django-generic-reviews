from django.conf import settings

from django.db import models


def get_review_app_models_settings():
    app_models = getattr(settings, "GENERIC_REVIEWS_APP_MODELS", None)
    if type(app_models) in [list, tuple]:
        return app_models
    else:
        raise ValueError(
            "GENERIC_REVIEWS_APP_MODELS must be tuple or list of 'app.model'"
        )


def limit_choices():
    app_models = get_review_app_models_settings()
    query = models.Q()
    for app_model in app_models:
        app, model = app_model.split(".")
        q = models.Q(app_label=app, model=model)
        query |= q
    return query
