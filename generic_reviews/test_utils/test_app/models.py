from django.db import models


class Car(models.Model):
    """
    A sample model for creating objects to be reviewed.
    """

    name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)

    def __str__(self):
        return self.name
