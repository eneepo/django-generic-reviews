# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

app_name = "generic_reviews"
urlpatterns = [
    url(
        regex=r"^Review/~create/$",
        view=views.ReviewCreateView.as_view(),
        name="Review_create",
    ),
    url(
        regex=r"^Review/(?P<pk>\d+)/~delete/$",
        view=views.ReviewDeleteView.as_view(),
        name="Review_delete",
    ),
    url(
        regex=r"^Review/(?P<pk>\d+)/$",
        view=views.ReviewDetailView.as_view(),
        name="Review_detail",
    ),
    url(
        regex=r"^Review/(?P<pk>\d+)/~update/$",
        view=views.ReviewUpdateView.as_view(),
        name="Review_update",
    ),
    url(regex="^Review/$", view=views.ReviewListView.as_view(), name="Review_list",),
]
