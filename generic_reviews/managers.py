from typing import Any, List, Type, Dict

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db.models import QuerySet
from django.db import models


class ReviewManager(models.Manager):
    def add_review(self, item: object, user: Type[User], reviews: List[Dict]) -> Any:
        objs = []
        for review in reviews:
            objs.append(
                self.model(
                    object_id=item.id,
                    content_type=ContentType.objects.get_for_model(item),
                    user=user,
                    category=review.get("category"),
                    comment=review.get("comment", ""),
                    rating=review.get("rating"),
                )
            )

        reviews = super(ReviewManager, self).bulk_create(objs)
        return reviews

    def get_average_rating_item(self, item: object) -> float:
        ct = ContentType.objects.get_for_model(item)
        reviews = super(ReviewManager, self).filter(object_id=item.id, content_type=ct)
        average = reviews.aggregate(models.Avg("rating"))
        return average.get("rating__avg")

    def get_average_rating_item_user(self, item: object, user: Type[User]) -> float:
        ct = ContentType.objects.get_for_model(item)
        reviews = super(ReviewManager, self).filter(
            object_id=item.id, content_type=ct, user=user
        )
        average = reviews.aggregate(models.Avg("rating"))
        return average.get("rating__avg")

    def get_average_rating_item_category(
        self, item: object, category: Type["Category"]
    ) -> float:
        ct = ContentType.objects.get_for_model(item)
        reviews = super(ReviewManager, self).filter(
            object_id=item.id, content_type=ct, category=category
        )

        average = reviews.aggregate(models.Avg("rating"))
        return average.get("rating__avg")

    def get_reviews_for_an_item(self, item: object) -> QuerySet:
        ct = ContentType.objects.get_for_model(item)
        qs = super(ReviewManager, self).filter(object_id=item.id, content_type=ct)
        return qs
