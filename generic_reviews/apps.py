# -*- coding: utf-8
from django.apps import AppConfig


class GenericReviewsConfig(AppConfig):
    name = "generic_reviews"
