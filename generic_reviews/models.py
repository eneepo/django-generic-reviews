# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .managers import ReviewManager
from .helpers import limit_choices


class Category(models.Model):
    """
    A category for an review. Examples: support, quality, etc..
    """

    name = models.CharField(max_length=250,)
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_("Content type"),
        on_delete=models.CASCADE,
        limit_choices_to=limit_choices,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("review category")
        verbose_name_plural = _("review categories")


class BaseReviewAbstractModel(models.Model):
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_("Content type"),
        related_name="content_type_set_for_%(class)s",
        on_delete=models.CASCADE,
    )
    object_id = models.PositiveIntegerField(_("Object ID"))
    content_object = GenericForeignKey(ct_field="content_type", fk_field="object_id")

    class Meta:
        abstract = True


class BaseRatingAbstractModel(models.Model):
    """
    Abstract class for user rating for an object.
    """

    rating = models.PositiveSmallIntegerField(_("Rating"))

    class Meta:
        abstract = True


class Review(BaseReviewAbstractModel, BaseRatingAbstractModel):
    """
    Abstract class for user reviewing an object.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("User"),
        blank=True,
        null=True,
        related_name="%(class)s_reviews",
        on_delete=models.SET_NULL,
    )
    category = models.ForeignKey(
        Category,
        verbose_name=_("Category"),
        related_name="%(class)s_reviews",
        on_delete=models.CASCADE,
    )
    comment = models.TextField("Comment")
    submit_date = models.DateTimeField(
        _("Submitted at"), auto_now_add=True, db_index=True
    )
    update_date = models.DateTimeField(_("Updated at"), auto_now=True, db_index=True)
    ip_address = models.GenericIPAddressField(
        _("IP address"), unpack_ipv4=True, blank=True, null=True
    )

    objects = ReviewManager()

    class Meta:
        ordering = ("-submit_date",)
        verbose_name = _("review")
        verbose_name_plural = _("reviews")
        unique_together = [
            ("object_id", "content_type", "user", "category"),
        ]

    def __str__(self):
        return "{} rated {!s} for the {} of {}".format(
            self.user, self.rating, self.category, self.content_object
        )
