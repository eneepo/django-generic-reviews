=====
Usage
=====

To use Django Generic Reviews in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'generic_reviews.apps.GenericReviewsConfig',
        ...
    )

Add Django Generic Reviews's URL patterns:

.. code-block:: python

    from generic_reviews import urls as generic_reviews_urls


    urlpatterns = [
        ...
        url(r'^', include(generic_reviews_urls)),
        ...
    ]
