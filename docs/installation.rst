============
Installation
============

At the command line::

    $ easy_install django-generic-reviews

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-generic-reviews
    $ pip install django-generic-reviews
