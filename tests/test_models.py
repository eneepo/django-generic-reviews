#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-generic-reviews
------------

Tests for `django-generic-reviews` models module.
"""

import random

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, override_settings

from generic_reviews.models import Category, Review

from .factories import CarFactory, CategoryFactory, ReviewFactory, UserFactory


class BaseCategoryTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BaseCategoryTestCase, cls).setUpClass()
        cls.ct = ContentType.objects.get(model="car")

        cls.speed = CategoryFactory(name="Speed")
        cls.value = CategoryFactory(name="Value")
        cls.interior = CategoryFactory(name="Interior")


class BaseReviewTestCase(BaseCategoryTestCase):
    @classmethod
    def setUpClass(cls):
        super(BaseReviewTestCase, cls).setUpClass()
        cls.user_1 = UserFactory()
        cls.user_2 = UserFactory()
        cls.user_3 = UserFactory()

        cls.car_1 = CarFactory()
        cls.car_2 = CarFactory()
        cls.car_3 = CarFactory()

        cls.reviews = {}

        for item in [cls.car_1, cls.car_2, cls.car_3]:
            user_2_review = cls.create_review_with_random_rating(item, cls.user_2)
            user_3_review = cls.create_review_with_random_rating(item, cls.user_3)
            cls.reviews[cls.user_2.username + "_" + item.name] = user_2_review
            cls.reviews[cls.user_3.username + "_" + item.name] = user_3_review

    @classmethod
    def create_review_with_random_rating(cls, item, user):
        review_obj = [
            item,
            user,
            [
                {"rating": random.randint(0, 5), "category": cls.speed},
                {"rating": random.randint(0, 5), "category": cls.value},
                {"rating": random.randint(0, 5), "category": cls.interior},
            ],
        ]

        Review.objects.add_review(*review_obj)
        return review_obj


class CategoryTestCase(BaseCategoryTestCase):
    def test_category_created_properly(self):
        self.assertEqual(str(self.speed), "Speed")
        self.assertEqual(self.speed.name, "Speed")
        self.assertIn(self.speed, Category.objects.all())

    @override_settings(GENERIC_REVIEWS_APP_MODELS=["test_app.website"])
    def test_assigning_correct_content_type_to_category(self):
        car = CarFactory()
        car_ct = ContentType.objects.get_for_model(car)
        category = Category()
        category.name = "Speed"
        category.content_type = car_ct
        category.save()
        self.assertTrue(
            Category.objects.filter(pk=category.pk, content_type=car_ct).exists()
        )


class ReviewTestCase(BaseReviewTestCase):
    def test_add_single_review(self):
        review = ReviewFactory.create()
        review.save()

        obj = Review.objects.get(
            object_id=review.object_id,
            content_type=review.content_type,
            user=review.user,
            category=review.category,
        )
        self.assertEqual(obj.rating, review.rating)

    def test_string_representation(self):
        rating = 4
        review = ReviewFactory(
            user=self.user_1,
            rating=rating,
            category=self.speed,
            object_id=self.car_1.id,
        )
        self.assertEqual(
            str(review),
            "{} rated {} for the {} of {}".format(
                self.user_1, rating, self.speed, self.car_1
            ),
        )

    def test_can_add_review_for_item(self):
        item = self.car_1
        user = self.user_1
        ct = ContentType.objects.get_for_model(self.car_1)

        Review.objects.add_review(
            item,
            user,
            [
                {
                    "rating": 3.8,
                    "comment": "Optional turbocharged engine provides quick acceleration",
                    "category": self.speed,
                },
                {
                    "rating": 4.5,
                    "comment": "Lots of features for your money",
                    "category": self.value,
                },
                {
                    "rating": 3.0,
                    "comment": "Interior is trimmed with a lot of hard plastic panels",
                    "category": self.interior,
                },
            ],
        )

        review_objs = Review.objects.filter(
            user=user, content_type=ct, object_id=self.car_1.id
        )
        self.assertEqual(review_objs.count(), 3)

    def test_calculate_user_average_rate_for_an_item(self):
        user = self.user_1
        item = self.car_1

        Review.objects.add_review(
            item,
            user,
            [
                {
                    "rating": 3.9,
                    "comment": "Clunky response from turbocharged engine's transmission",
                    "category": self.speed,
                },
                {
                    "rating": 4.0,
                    "comment": "Lots of features for the price",
                    "category": self.value,
                },
                {
                    "rating": 4.25,
                    "comment": "Roomier than other subcompact crossovers",
                    "category": self.interior,
                },
            ],
        )

        avg_rate = Review.objects.get_average_rating_item_user(item, user)
        self.assertAlmostEqual(avg_rate, 3.666666666)

    def test_calculate_average_rating_for_an_item(self):
        item = self.car_1
        reviews = self.reviews
        average = Review.objects.get_average_rating_item(item)
        total = 0.0

        for key, value in reviews.items():
            if key.endswith(item.name):
                for review in value[2]:
                    total += review["rating"]

        self.assertAlmostEqual(total / len(self.reviews), average)

    def test_calculate_average_rating_for_a_category(self):
        item = self.car_1
        reviews = self.reviews
        category = self.speed
        average = Review.objects.get_average_rating_item_category(item, category)
        total = 0.0
        count = 0

        for _, values in reviews.items():
            if values[0] is item:
                for review in values[2]:
                    if review["category"] is category:
                        total += review["rating"]
                        count += 1

        self.assertAlmostEqual(total / count, average)

    def test_getting_reviews_for_an_item(self):
        car = CarFactory(name="3", manufacturer="Mazda")
        ReviewFactory(
            object_id=car.id,
            user=UserFactory(username="aa"),
            category=CategoryFactory(name="Category A"),
        )
        ReviewFactory(
            object_id=car.id,
            user=UserFactory(username="aa"),
            category=CategoryFactory(name="Category B"),
        )
        ReviewFactory(
            object_id=car.id,
            user=UserFactory(username="bb"),
            category=CategoryFactory(name="Category A"),
        )
        ReviewFactory(
            object_id=car.id,
            user=UserFactory(username="cc"),
            category=CategoryFactory(name="Category A"),
        )
        total_reviews = Review.objects.filter(object_id=car.id).count()
        self.assertEqual(total_reviews, 4)
