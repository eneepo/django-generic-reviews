from django.test import TestCase
from generic_reviews.test_utils.test_app.apps import TestAppConfig


class TestAppConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(TestAppConfig.name, "test_app")
