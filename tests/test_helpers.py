from django.test import TestCase, override_settings
from django.db import models

from generic_reviews.helpers import limit_choices


class LimitChoicesTestCase(TestCase):
    def test_limit_choices_accepts_list_tuple(self):
        param_list = [["test_app.car"], ("test_app.car",)]
        for param in param_list:
            with self.settings(GENERIC_REVIEWS_APP_MODELS=param):
                with self.subTest():
                    qs = models.Q(app_label="test_app", model="car")
                    self.assertEqual(limit_choices(), qs)

    def test_limit_choices_accepts_returns_correct_qs(self):
        param_list = ["test_app.car", "test_app.website"]
        with self.settings(GENERIC_REVIEWS_APP_MODELS=param_list):
            qs = models.Q(app_label="test_app", model="car") | models.Q(
                app_label="test_app", model="website"
            )
            self.assertEqual(limit_choices(), qs)

    def test_limit_choices_only_raise_exception_incorrect_app_models(self):

        param_list = ["", {}, 5, 4.5, None, True, False, range(3)]
        for param in param_list:
            with self.settings(GENERIC_REVIEWS_APP_MODELS=param):
                with self.subTest():
                    with self.assertRaisesMessage(
                        ValueError,
                        "GENERIC_REVIEWS_APP_MODELS must be tuple or list of 'app.model'",
                    ):
                        limit_choices()

    @override_settings(GENERIC_REVIEWS_APP_MODELS="app.model")
    def test_limit_choices_exception_message(self):
        with self.assertRaisesMessage(
            ValueError,
            "GENERIC_REVIEWS_APP_MODELS must be tuple or list of 'app.model'",
        ):
            limit_choices()
