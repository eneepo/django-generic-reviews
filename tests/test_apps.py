from django.apps import apps
from django.test import TestCase
from generic_reviews.apps import GenericReviewsConfig


class ReviewConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(GenericReviewsConfig.name, "generic_reviews")
        self.assertEqual(apps.get_app_config("generic_reviews").name, "generic_reviews")
