import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "auth.user"
        django_get_or_create = ("username",)

    username = factory.Faker("user_name")
