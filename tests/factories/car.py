import factory
from faker import Faker
from faker.providers import BaseProvider


fake = Faker()


class CarProvider(BaseProvider):
    cars = [
        ("Audi", "A1", 2010),
        ("Audi", "A3", 2003),
        ("Audi", "A5", 2003),
        ("Audi", "A6", 2011),
        ("Audi", "A6 allroad quattro", 2012),
        ("Audi", "A7", 2010),
        ("Audi", "A8", 2010),
        ("Audi", "Q2", 2017),
        ("Audi", "Q3", 2011),
        ("Audi", "Q5", 2008),
        ("Audi", "Q7", 2005),
        ("Audi", "Q8", 2008),
        ("Audi", "R8", 2015),
        ("Audi", "TT", 2017),
        ("Benz", "Mercedes-AMG One", 2020),
        ("Benz", "X247 GLB-Class", 2019),
        ("Benz", "C118 CLA-Class", 2019),
        ("Benz", "W247 B-Class", 2019),
        ("Benz", "W167 GLE-Class", 2019),
        ("Benz", "EQC", 2019),
        ("Benz", "W463 G-Class", 2018),
        ("Benz", "️Mercedes-AMG GT 4-Door Coupé", 2019),
        ("Kia", "Soul", 2008),
        ("Kia", "Seltos", 2019),
        ("Kia", "Sportage", 1993),
        ("Kia", "Sorento", 2002),
    ]

    def car(self):
        return self.random_element(self.cars)


fake.add_provider(CarProvider)
factory.Faker.add_provider(CarProvider)


class CarFactory(factory.DjangoModelFactory):
    class Meta:
        model = "test_app.Car"
        django_get_or_create = ("name",)
        exclude = ("car",)

    car = factory.Faker("car")
    manufacturer = factory.LazyAttribute(lambda o: o.car[0])
    name = factory.LazyAttribute(lambda o: o.car[1])
