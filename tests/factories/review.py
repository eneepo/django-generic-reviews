import random
import factory
from factory import fuzzy

from django.contrib.contenttypes.models import ContentType

from .car import CarFactory
from .user import UserFactory


class CategoryFactory(factory.DjangoModelFactory):
    class Meta:
        model = "generic_reviews.Category"
        django_get_or_create = ("name", "content_type")

    name = random.choice(
        ["Speed", "Value", "Interior", "Economical", "Tech", "Storage"]
    )
    content_type = factory.LazyAttribute(lambda o: ContentType.objects.get(model="car"))


class ReviewFactory(factory.DjangoModelFactory):
    class Meta:
        model = "generic_reviews.Review"
        django_get_or_create = (
            "object_id",
            "content_type",
            "user",
            "category",
            "rating",
        )
        exclude = ("object",)

    user = factory.SubFactory(UserFactory)
    object = factory.SubFactory(CarFactory)
    object_id = factory.SelfAttribute("object.id")
    content_type = factory.LazyAttribute(lambda o: ContentType.objects.get(model="car"))
    comment = factory.Faker("paragraph")
    ip_address = factory.Faker("ipv4")
    category = factory.SubFactory(CategoryFactory)
    rating = fuzzy.FuzzyInteger(0, 5)
