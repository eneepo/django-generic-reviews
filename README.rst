=============================
Django Generic Reviews
=============================

.. image:: https://badge.fury.io/py/django-generic-reviews.svg
    :target: https://badge.fury.io/py/django-generic-reviews

.. image:: https://gitlab.com/eneepo/django-generic-reviews/badges/master/pipeline.svg
    :target: https://gitlab.com/eneepo/django-generic-reviews/pipelines

.. image:: https://codecov.io/gl/eneepo/django-generic-reviews/branch/master/graph/badge.svg
    :target: https://codecov.io/gl/eneepo/django-generic-reviews

.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
    :target: https://github.com/pre-commit/pre-commit

A generic app to add review and rating to any Django model.

Documentation
-------------

The full documentation is at https://django-generic-reviews.readthedocs.io.

Quickstart
----------

Install Django Generic Reviews::

    pip install django-generic-reviews

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'generic_reviews.apps.GenericReviewsConfig',
        ...
    )

Add Django Generic Reviews's URL patterns:

.. code-block:: python

    from generic_reviews import urls as generic_reviews_urls


    urlpatterns = [
        ...
        url(r'^', include(generic_reviews_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
